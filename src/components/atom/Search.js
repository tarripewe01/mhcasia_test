import React from 'react';
import {StyleSheet, TextInput} from 'react-native';

const Search = props => {
  return (
    <>
      <TextInput
        placeholder="Search here"
        value={props.value}
        onChangeText={props.onChangeText}
        style={styles.input}
      />
    </>
  );
};

export default Search;

const styles = StyleSheet.create({
  input: {
    borderWidth: 1,
    borderColor: 'black',
    marginTop: 30,
    marginBottom: 15,
    paddingHorizontal: 20,
    width: 350,
    height: 40,
    borderRadius: 5,
  },
});
