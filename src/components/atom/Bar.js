import React from 'react';
import {StatusBar} from 'react-native';

const Bar = ({barStyle, color}) => {
  return (
    <>
      <StatusBar barStyle={barStyle} backgroundColor={color} />
    </>
  );
};

export default Bar;
