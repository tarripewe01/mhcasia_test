import React from 'react';
import {View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const ButtonBookmark = ({name}) => {
  return (
    <View>
      <Icon name={name} color="white" size={30} />
    </View>
  );
};

export default ButtonBookmark;
