import React, {useContext, useEffect, useState} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Hooks} from '../../context/provider';
import ButtonBookmark from '../atom/ButtonBookmark';

const CardItem = ({onPress, image, name, description, category, booked}) => {
  const {handleSetBook, data} = useContext(Hooks);
  const [bookmark, setBookmark] = useState(false);

  const addToBookmark = () => {
    console.log('added');
    setBookmark(!bookmark);
  };
  useEffect(() => {
  }, [data]);
  return (
    <TouchableOpacity onPress={onPress} style={styles.page}>
      <View style={styles.containImage}>
        <ImageBackground
          source={{
            uri: image,
          }}
          style={styles.image}>
          <View style={styles.icon}>
            <TouchableOpacity
              onPress={e => {
                addToBookmark;
                handleSetBook(name);
              }}>
              <ButtonBookmark name={booked ? 'bookmark' : 'bookmark-outline'} />
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
      <View style={styles.content}>
        <Text>Name: {name}</Text>
        <Text>Description: {description}</Text>
        <Text>Category: {category}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default CardItem;

const styles = StyleSheet.create({
  page: {
    backgroundColor: 'white',
    width: 170,
    height: 170,
    marginRight: 10,
    marginBottom: 20,
    borderWidth: 1,
    borderColor: 'black',
  },
  containImage: {
    backgroundColor: 'black',
    height: 80,
    alignItems: 'center',
    paddingVertical: 5,
  },
  image: {
    width: 70,
    height: 70,
  },
  icon: {marginLeft: -40, marginTop: -5},
  content: {paddingHorizontal: 10, paddingTop: 10},
});
