import React, {useContext, useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Hooks} from '../../context/provider';

const Filter = props => {
  const {filterCategory} = useContext(Hooks);
  console.log(filterCategory, 'filtercategory');

  useEffect(() => {}, [filterCategory]);
  return (
    <View style={styles.page}>
      <TouchableOpacity
        onPress={props.btnAll}
        style={{
          backgroundColor: filterCategory === 'data' ? 'white' : 'red',
          borderColor: filterCategory !== 'data' ? 'white' : 'red',
          borderWidth: 1,
          width: 70,
          borderRadius: 10,
          alignItems: 'center',
          height: 30,
          marginRight: 10,
        }}>
        <Text
          style={{
            color: filterCategory === 'data' ? 'red' : 'white',
          }}>
          All
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={props.btnOdd}
        style={{
          backgroundColor: filterCategory === 'odd' ? 'white' : 'red',
          borderColor: filterCategory !== 'odd' ? 'white' : 'red',
          borderWidth: 1,
          width: 70,
          borderRadius: 10,
          alignItems: 'center',
          height: 30,
          marginRight: 10,
        }}>
        <Text
          style={{
            color: filterCategory === 'odd' ? 'red' : 'white',
          }}>
          odd
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={props.btnEven}
        style={{
          backgroundColor: filterCategory === 'even' ? 'white' : 'red',
          borderColor: filterCategory !== 'even' ? 'white' : 'red',
          borderWidth: 1,
          width: 70,
          borderRadius: 10,
          alignItems: 'center',
          height: 30,
          marginRight: 10,
        }}>
        <Text
          style={{
            color: filterCategory === 'even' ? 'red' : 'white',
          }}>
          even
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Filter;

const styles = StyleSheet.create({
  page: {marginBottom: 20, flexDirection: 'row'},
});
