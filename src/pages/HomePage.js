import React, {useEffect, useState, useContext} from 'react';
import {StyleSheet, FlatList, SafeAreaView} from 'react-native';

import Bar from '../components/atom/Bar';
import CardItem from '../components/molecules/CardItem';
import Filter from '../components/molecules/Filter';
import Search from '../components/atom/Search';

import {apiURL} from '../helper/apiURL';
import {Hooks} from '../context/provider';

const HomePage = ({navigation}) => {
  const {
    data,
    setData,
    _storeData,
    dataOdd,
    setDataOdd,
    dataEven,
    setDataEven,
    dataSearch,
    setDataSearch,
    filterCategory,
    setFilterCategory,
  } = useContext(Hooks);
  const [filterData, setFilterData] = useState([]);
  const [masterData, setMasterData] = useState([]);
  const [search, setSearch] = useState('');

  useEffect(() => {
    fetchPosts();
    return () => {};
  }, []);

  // start get ALl Data from API
  const fetchPosts = () => {
    fetch(apiURL)
      .then(response => response.json())
      .then(responseJson => {
        setFilterData(responseJson);
        setMasterData(responseJson);
        _storeData(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  };
  // end get ALl Data from API

  //   start Search Fungsional
  const searchFilter = e => {
    setFilterCategory('search');
    if (e) {
      const newData = data.filter(item => {
        const itemData = item.id ? item.id.toUpperCase() : ''.toUpperCase();
        const textData = e.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setDataSearch(newData);
      setSearch(e);
    } else {
      setFilterData(data);
      setSearch(e);
    }
  };
  //   end Search Fungsional

  //   start handle button category
  const handleBtnAll = e => {
    setFilterCategory('data');
    setSearch(e);
  };

  const handleBtnOdd = e => {
    if (e) {
      const newFilterCategory = data.filter(item => {
        const itemCategory = item.category_name;
        return itemCategory === 'odd';
      });
      setDataOdd(newFilterCategory);
      setFilterCategory('odd');
    } else {
      setFilterData(data);
      setFilterCategory(e);
    }
  };
  const destructData = val => {
    if (val === 'odd') {
      return dataOdd;
    } else if (val === 'even') {
      return dataEven;
    } else if (val === 'search') {
      return dataSearch;
    } else {
      return data;
    }
  };
  useEffect(() => {}, [data, filterData, filterCategory]);
  const handleBtnEven = e => {
    if (e) {
      const newFilterCategory = data.filter(item => {
        const itemCategory = item.category_name;
        return itemCategory === 'even';
      });
      setDataEven(newFilterCategory);
      setFilterCategory('even');
    } else {
      setFilterData(data);
      setFilterCategory('even');
    }
  };
  //   end handle button category

  //   start Card Item Props

  const CardView = ({item}) => {
    return (
      <>
        <CardItem
          image={item.url}
          name={item.id}
          booked={item?.is_book}
          description={item.category}
          category={item.category_name}
          onPress={() =>
            navigation.push('DetailPage', {
              image: item.url,
              booked: item?.is_book,
            })
          }
        />
      </>
    );
  };
  //   end Card Item Props

  return (
    <>
      <Bar barStyle="dark-content" color="white" />
      <SafeAreaView style={styles.page}>
        <Search value={search} onChangeText={e => searchFilter(e)} />
        <Filter
          btnAll={handleBtnAll}
          btnOdd={handleBtnOdd}
          btnEven={handleBtnEven}
        />
        <FlatList
          data={destructData(filterCategory)}
          keyExtractor={(item, index) => index.toString()}
          renderItem={CardView}
          numColumns={2}
          showsVerticalScrollIndicator={false}
        />
      </SafeAreaView>
    </>
  );
};

export default HomePage;

const styles = StyleSheet.create({
  page: {
    backgroundColor: 'white',
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 20,
  },
});
