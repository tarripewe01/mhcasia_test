import React, {useState} from 'react';
import {
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
} from 'react-native';
import ImageZoom from 'react-native-image-pan-zoom';
import Icon from 'react-native-vector-icons/FontAwesome5';

import Bar from '../components/atom/Bar';
import ButtonBookmark from '../components/atom/ButtonBookmark';

const DetailImage = ({navigation, route}) => {
  const [bookmark, setBookmark] = useState(false);

  const addToBookmark = () => {
    console.log('added');
    setBookmark(!bookmark);
  };

  const handleBack = () => navigation.goBack('Home');
  return (
    <>
      <Bar barStyle="light-content" color="black" />
      <View style={styles.page}>
        <View style={styles.containIcon}>
          <TouchableOpacity onPress={handleBack}>
            <Icon name="arrow-left" color="white" size={30} />
          </TouchableOpacity>
          <TouchableOpacity onPress={addToBookmark}>
            <ButtonBookmark
              name={route.params.booked ? 'bookmark' : 'bookmark-outline'}
            />
          </TouchableOpacity>
        </View>
        <ImageZoom
          cropWidth={Dimensions.get('window').width}
          cropHeight={Dimensions.get('window').height / 1.3}
          imageWidth={200}
          imageHeight={200}>
          <Image
            source={{
              uri: route.params.image,
            }}
            style={styles.image}
          />
        </ImageZoom>
      </View>
    </>
  );
};

export default DetailImage;

const styles = StyleSheet.create({
  page: {
    backgroundColor: 'black',
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 20,
  },
  containIcon: {flexDirection: 'row', justifyContent: 'space-between'},
  image: {width: 200, height: 200},
});
