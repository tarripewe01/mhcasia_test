import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import HomePage from './pages/HomePage';
import DetailImage from './pages/DetailImage';
import Hooks from './context/provider'
const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <Hooks>
      <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomePage}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="DetailPage"
          component={DetailImage}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
    </Hooks>
  );
};

export default App;
