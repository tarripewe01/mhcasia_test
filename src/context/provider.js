import React, {createContext, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
export const Hooks = createContext();
const Provider = props => {
  const [data, setData] = useState({});
  const [dataOdd, setDataOdd] = useState({});
  const [dataEven, setDataEven] = useState({});
  const [dataSearch, setDataSearch] = useState({});
  const [filterCategory, setFilterCategory] = useState('data');

  const [bookId, setBookId] = useState({});
  const handleSetBook = val => {
    const newData = [...data];
    try {
      newData.map((valx, i) => {
        if (valx.id === val) {
          newData[i] = {
            ...newData[i],
            is_book:
              newData[i].is_book !== undefined
                ? newData[i].is_book
                  ? false
                  : true
                : true,
          };
          console.log(newData[i], 'cek langa');
          setData(newData);
          AsyncStorage.setItem('data', newData);
        }
      });
    } catch (error) {}
  };

  const _storeData = async val => {
    setData(val);
    try {
      await AsyncStorage.setItem('data', val);
    } catch (error) {
      console.log(error);
    }
  };
  const valx = {
    data,
    setData,
    _storeData,
    handleSetBook,
    bookId,
    setBookId,
    dataOdd,
    setDataOdd,
    dataEven,
    setDataEven,
    dataSearch,
    setDataSearch,
    filterCategory,
    setFilterCategory,
  };
  return <Hooks.Provider value={valx}>{props.children}</Hooks.Provider>;
};

export default Provider;
