# MHCAsia_Test

## Platform
. Android

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Flow Chart
<img src="/uploads/5e0558e5035792087d4bf6813bb98ad8/Untitled_Diagram.jpg" width="90%"></img> 

## Demo
<img src="/uploads/053129aae5db354cae4ab6b48d8f74c3/2e5580e7-464e-492b-8695-ed6c53ee4555.mp4" width="45%"></img> 

## Screenshots
<img src="/uploads/215348c43cf5b249edea2d005eb69fa8/photo6266814518948638071.jpg" width="45%"></img> <img src="/uploads/dba937e89ff607559d06fe6b4e46d946/photo6266814518948638072.jpg" width="45%"></img> <img src="/uploads/06218f61ca0aa46dad701d092225eaa5/photo6266814518948638073.jpg" width="45%"></img> <img src="/uploads/05d2a30273705239aae5532be4af23e4/photo6266814518948638074.jpg" width="45%"></img> <img src="/uploads/7245c4b1a18ec7a6e06edff087144c58/photo6266814518948638075.jpg" width="45%"></img> 
